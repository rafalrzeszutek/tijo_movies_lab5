package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;
import pl.edu.pwsztar.utils.converter.Converter;

@Component
public class MovieMapper implements Converter<CreateMovieDto, Movie> {


    @Override
    public Movie convert(CreateMovieDto from) {
        Movie movie = new Movie();

        movie.setTitle(from.getTitle());
        movie.setImage(from.getImage());
        movie.setYear(from.getYear());

        return movie;
    }
}
